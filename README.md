This project allows you to stream directly to your peers, withour uploading to eg. Twitch first.

I hoped this would minimize the delay, unfortunately a few seconds delay still remains, eg. 3 to 5 seconds.

## Setup
Just start the included nginx server.

## Testing
rtmp://192.168.0.186:1935/live/stream


## Playback: Flash player based:
* Get the included FlashRTMPPlayer folder
* Enable Flash
* Stream: rtmp://192.168.0.186:1935/live
* Stream name: stream

* or use https://www.wowza.com/testplayers

## Playback: VLC Player
* Works with VLC player as well, I didn't use it because it has a higher delay then the Flash based viewer.
* Media -> "Open Network Stream" -> rtmp://192.168.0.186:1935/live/stream
